<?php

///
// Enqueue search javascript
///
function enqueue_system_search_scripts() {

  // Load js only for 'Find a sitter page'
  $options = get_option( 'hs_theme_display_options' );
  $page_name = $options['select_find_a_sitter'];
  if(is_page($page_name)){
    wp_enqueue_script( 'search-js', get_stylesheet_directory_uri() . '/system/js/search.min.js', array('jquery'));
    wp_localize_script( 'search-js', 'ajax_object',
              array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'search_nonce' => wp_create_nonce( 'search_nonce' ) ) );
  }
}

add_action( 'wp_enqueue_scripts', 'enqueue_system_search_scripts' );
add_action( 'wp_ajax_system_search_action', 'system_search' );
add_action( 'wp_ajax_nopriv_system_search_action', 'system_search' );

add_action( 'wp_ajax_system_get_languages_by_city_action', 'system_get_languages_by_city' );
add_action( 'wp_ajax_nopriv_system_get_languages_by_city_action', 'system_get_languages_by_city' );


///
// Handle sitter search results
///
function system_search(){

  //Check for nonce security
if(!isset( $_POST['search_nonce'] ) || ! wp_verify_nonce( $_POST['search_nonce'], 'search_nonce' )  ){
echo "ERROR INVALID WP_NONCE";
return;
}

  // Map hours with values
  $hours_map = array(
    '00:00'=>0,'00:30'=>0.5,'01:00'=>1,'01:30'=>1.5,'02:00'=>2,'02:30'=>2.5,'03:00'=>3,'03:30'=>3.5,'04:00'=>4,'04:30'=>4.5,'05:00'=>5,
    '05:30'=>5.5,'06:00'=>6,'06:30'=>6.5,'07:00'=>7,'07:30'=>7.5,'08:00'=>8,'08:30'=>8.5,'09:00'=>9,'09:30'=>9.5,'10:00'=>10,'10:30'=>10.5,
    '11:00'=>11,'11:30'=>11.5,'12:00'=>12,'12:30'=>12.5,'13:00'=>13,'13:30'=>13.5,'14:00'=>14,'14:30'=>14.5,'15:00'=>15,'15:30'=>15.5,'16:00'=>16,
    '16:30'=>16.5,'17:00'=>17,'17:30'=>17.5,'18:00'=>18,'18:30'=>18.5,'19:00'=>19,'19:30'=>19.5,'20:00'=>20,'20:30'=>20.5,'21:00'=>21,'21:30'=>21.5,
    '22:00'=>22,'22:30'=>22.5,'23:00'=>23,'23:30'=>23.5,'24:00' => 0
  );

  // Collect data
  $timefrom = $_POST['timefrom'];
  $city = $_POST['city'];
  $duration = $_POST['duration'];
  $duration_hours_minutes = explode('.', $duration);
  $duration_hours = $duration_hours_minutes[0];
  $duration_minutes = (isset($duration_hours_minutes[1]) AND $duration_hours_minutes[1] == '5')? '30':'0';
  $date = $_POST['date'];
  $previous_date = new DateTime($date);
  $previous_date->modify('-1 Days');
  $previous_date = $previous_date->format('Y-m-d');
  $next_date = new DateTime($date);
  $next_date->modify('+1 Days');
  $next_date = $next_date->format('Y-m-d');

  $days_in_week = array('Sunday'=>'Su', 'Monday'=>'Mo', 'Tuesday'=>'Tu', 'Wednesday'=>'We', 'Thursday'=>'Th', 'Friday'=>'Fr', 'Saturday'=>'Sa');
  $day_of_week = date("l", strtotime($date));
  $day_of_week = $days_in_week[$day_of_week];
  $day_of_week_next = array('Su'=>'Mo', 'Mo'=>'Tu', 'Tu'=>'We', 'We'=>'Th', 'Th'=>'Fr', 'Fr'=>'Sa', 'Sa'=>'Su');

  $childrens = $_POST['childrens']; // array( array('english','toddler'), array('German','toddler') )
  $children_language = array();
  $children_age = array();
  $availability_array = array();


  // Dependencies from booking options page
  $booking_options_page = get_option( 'hs_theme_booking_options' );
  $minimum_time_beetwen_first_booking = $booking_options_page['find_sitters_minimum_time_booking'];
  $minimum_time_beetwen_first_booking = explode('.', $minimum_time_beetwen_first_booking);
  $minimum_time_beetwen_first_booking_hours = $minimum_time_beetwen_first_booking[0];
  $minimum_time_beetwen_first_booking_minutes = ( !isset($minimum_time_beetwen_first_booking[1]) )?'0':'30';
  $minimum_time_beetwen_bookings = $booking_options_page['find_sitters_minimum_time_between_bookings'];
  $minimum_time_beetwen_bookings = explode('.', $minimum_time_beetwen_bookings);
  $minimum_time_beetwen_bookings_hours = $minimum_time_beetwen_bookings[0];
  $minimum_time_beetwen_bookings_minutes = ( !isset($minimum_time_beetwen_bookings[1]) )? '0':'30';
  $maximum_booking_days_in_front = $booking_options_page['find_sitters_max_booking_days'];


  // If timefrom is 24 switch date to next date
  $time = array_search($timefrom, $hours_map);
  if($time == 0){
    $time_range_start = new DateTime( $next_date. ' '. $time );
    $time_range_ends = new DateTime( $next_date. ' '. $time );

  // If its not date stays the same
  }else{
    $time_range_start = new DateTime( $date. ' '. $time );
    $time_range_ends = new DateTime( $date. ' '. $time );
  }
  $time_range_ends->modify('+' . $duration_hours .' hours');
  $time_range_ends->modify('+' . $duration_minutes .' minutes');


  // Check is selected date is in the past
  $date_and_time_is_valid = false;
  $time_at_this_moment = new DateTime();
  $time_at_this_moment->modify('+ ' . $minimum_time_beetwen_first_booking_hours . ' hours');
  $time_at_this_moment->modify('+ ' . $minimum_time_beetwen_first_booking_minutes . ' minutes');
  if( $time_range_start->getTimestamp() >= $time_at_this_moment->getTimestamp() ){
    $date_and_time_is_valid = true;
  }


  // Check if selected date is in time range for the future
  $date_and_time_is_valid_in_future = false;
  $time_at_this_moment->modify('+ ' . $maximum_booking_days_in_front . ' days');
  if( $time_range_start->getTimestamp() <= $time_at_this_moment->getTimestamp() ){
    $date_and_time_is_valid_in_future = true;
  }

  // Get customer time
  $customer_time = array('start' => floatval($timefrom), 'end' => floatval($timefrom) + floatval($duration));

  // All sitters time
  $sitters_time = array(
    array('start'=>6,'end'=>9), array('start'=>9,'end'=>12), array('start'=>12,'end'=>15),
    array('start'=>15,'end'=>18), array('start'=>18,'end'=>21), array('start'=>21,'end'=>24),
    array('start'=>24,'end'=>30), array('start'=>30,'end'=>32)
  );

  // Generate avaliabillity based on customer input
  // This is the logic part
  foreach ($sitters_time as $sitter_time) {

    // If sitters time is in range of customer time add it to array
    if( $sitter_time['start'] < $customer_time['end'] AND $sitter_time['end'] > $customer_time['start']){

          // Get next day if time goes after 24:00
    if($sitter_time['start'] == 24 AND $sitter_time['end'] == 30){

            // Get next day of current day and time
    $time = $day_of_week_next[$day_of_week] . "_0-6";

    }elseif($sitter_time['start'] == 30 AND $sitter_time['end'] == 32){

            // Get next day of current day and time
            $time = $day_of_week_next[$day_of_week] . "_6-9";

    }else{

            // Get time to store in array
    $time = $day_of_week . "_" . $sitter_time['start'] . "-" . $sitter_time['end'];
    }

    array_push($availability_array, $time);
}
  }

  // Create meta query condition based on avaliabillity field array
  $meta_query = array('relation' => 'AND');
  foreach ($availability_array as $time) {
      array_push( $meta_query,array( // Avaliabillity field
        'key' => '_general_availability',
        'value'   => $time,
        'compare' => 'LIKE'
      )
    );
  }
  array_push( $meta_query, array( // Status field
    'key' => 'sitter_status',
    'value'   => 'active',
    'compare' => '='
    )
  );

  $price_per_hour = $booking_options_page['booking_default_price'];
  $total_price = $price_per_hour * $duration;
  foreach($childrens as $children){
    array_push($children_language, $children[0]);
    array_push($children_age, $children[1]);
  }

  // WP_Query search params
  $args = array(
    'post_type'  => 'hs_sitter', // Post type
    'order'      => 'ASC', // Order,
    'tax_query' => array(

      // Filter by language
      array(
  'taxonomy' => 'language',
  'field'    => 'slug',
        'terms' => $children_language,
        'operator' => 'AND'
  ),

      // Filter by location
      array(
  'taxonomy' => 'location',
  'field'    => 'slug',
        'terms' => $city
  )
  ),

    // Filter by meta query, eg avaliabillity
    'meta_query' => $meta_query
  );


  // If date is in the past show this message
  if(!$date_and_time_is_valid){
    echo "<div class='container'>".__('Sorry, you can\'t search for sitters in the past, or time is to close for sitter to be booked. Please select time in the future.')."</div>";
    exit;return;
  }

  // If date is not in maximum range from options, show this message
  if(!$date_and_time_is_valid_in_future){
    echo "<div class='container'>".__('Sorry, you can search for maximum '.$maximum_booking_days_in_front.' days in front.')."</div>";
    exit;return;
  }

  ///
  // The main loop that returns sitters
  ///
  $the_query = new WP_Query( $args );

  if ( $the_query->have_posts() ) {

    echo '<div class="container"><ul>';

    while ( $the_query->have_posts() ) {
      $the_query->the_post();

      ///
      // Check booking avilability for sitter for selected time
      ///

      // Maximum working hours in one day
      $max_daily_sitting_time = $booking_options_page['find_sitters_max_daily_sitting_time'];

      // Get all booking times and store it in array
      $sitter_booking_times_array = array();
      $sitter_booking_times = get_field('sitter_booking_times', get_the_ID());
      foreach ($sitter_booking_times as $sitter_booking_time) {
        array_push($sitter_booking_times_array, $sitter_booking_time['time']);
      }

      //  This array would store time that we gonna compare. Format looks like this:
      //  array('previous_time' => '2017-11-15-08:00',
      //        'current_time' => array(array('2017-11-15-12:00_2017-11-15-15:00'), array('2017-11-16-02:30_2017-11-16-04:30'))
      //        'next_time' => '2017-11-16-10:00' )
      $data_for_comparing = array("previous_time" => "", "current_time" => array(), "next_time" => "");

      // Populate $data_for_comparing
      foreach ($sitter_booking_times_array as $sitter_booking_time) {

        // Get dates and times from booking time
        $sitter_booking_time_from_string = strtr( $sitter_booking_time, array('_' => ' ', '-' => ' ') );
        $sitter_booking_time_from_pieces = explode(' ', $sitter_booking_time_from_string);
        $year_from = $sitter_booking_time_from_pieces[0];
        $month_from = $sitter_booking_time_from_pieces[1];
        $day_from = $sitter_booking_time_from_pieces[2];
        $time_from = $sitter_booking_time_from_pieces[3];
        $year_to = $sitter_booking_time_from_pieces[4];
        $month_to = $sitter_booking_time_from_pieces[5];
        $day_to = $sitter_booking_time_from_pieces[6];
        $time_to = $sitter_booking_time_from_pieces[7];

        // Get first time before 9 hours
        if($year_to.'-'.$month_to.'-'.$day_to == $date AND (float)$hours_map[$time_to] < 9){
          $data_for_comparing['previous_time'] = $date.'-'.$time_to;
        }

        // Get all booking times for current date
        if( $year_to.'-'.$month_to.'-'.$day_to == $date AND (float)$hours_map[$time_to] > 9){
          array_push($data_for_comparing['current_time'], array($sitter_booking_time));
        }
        if ($year_to.'-'.$month_to.'-'.$day_to == $next_date AND (float)$hours_map[$time_to] < 9 ){
          array_push($data_for_comparing['current_time'], array($sitter_booking_time));
        }

        // Get first time after 9 hours
        if($year_from.'-'.$month_from.'-'.$day_from == $next_date AND (float)$hours_map[$time_from] > 9){
          if($data_for_comparing['next_time'] == ""){
            $data_for_comparing['next_time'] = $next_date.'-'.$time_from;
          }
        }
      }
      // var_dump($data_for_comparing);

      // This variable would store data for comparing in this format:
      // array('2017-11-15-08:00', '2017-11-15-12:00', '2017-11-15-15:00', '2017-11-16-02:30');
      $data_for_comparing_remove_arrays= array();
      foreach ($data_for_comparing as $key => $time) {

        // If its 'current_time'
        if(is_array($time)){
          foreach ($time as $key2 => $time_in_array) {

            // Split times from 'current_time'
            $split = strtr( $time_in_array[0], array('_' => ' ', '-' => ' ') );
            $split = explode(' ', $split);
            $year_from = $split[0]; $month_from = $split[1]; $day_from = $split[2]; $time_from = $split[3];
            $year_to = $split[4]; $month_to = $split[5]; $day_to = $split[6]; $time_to = $split[7];

            // Compare date from and date to to find out how menu hours sitter has booked for specific order
            $date_from = new DateTime($year_from.'-'.$month_from.'-'.$day_from.' '.$time_from);
            $date_to = new DateTime($year_to.'-'.$month_to.'-'.$day_to.' '.$time_to);
            $timestamp = $date_to->getTimestamp() - $date_from->getTimestamp();
            $time_to_reduce = new DateTime('@' . $timestamp);
            $time_to_reduce = $time_to_reduce->format('H.i');
            $time_to_reduce = strtr($time_to_reduce, array('30'=>'5'));

            // Substract from max sitting time, time that is alredy used.
            $max_daily_sitting_time -= $time_to_reduce;

            // Push times from 'current_time' to array
            $split1 = explode('_', $time_in_array[0]);
            array_push($data_for_comparing_remove_arrays, $split1[0]);
            array_push($data_for_comparing_remove_arrays, $split1[1]);
          }

        // If its 'previous_time' or 'next_time'
        }else{
          array_push($data_for_comparing_remove_arrays, $time);
        }
      }
      // var_dump($data_for_comparing_remove_arrays);

      // If 'previous_time' is empty add time to be current date 09:00 time
      // If 'next_time' is empty string, add time to be next date 09:00 time
      if($data_for_comparing_remove_arrays[0] == ''){
          $data_for_comparing_remove_arrays[0] = $date.' 09:00';
      }
      if($data_for_comparing_remove_arrays[count($data_for_comparing_remove_arrays)-1] == ''){
        $data_for_comparing_remove_arrays[count($data_for_comparing_remove_arrays)-1] = $next_date.' 09:00';
      }

      // Create from array('2017-11-15-08:00', '2017-11-15-12:00', '2017-11-15-15:00', '2017-11-16-02:30); key and values
      // array('2017-11-15 08:00' => '2017-11-15-12:00', 2017-11-15 15:00'=>'2017-11-16-02:30'); with added time beetwen sitting difference
      $data_for_comparing_key_values = array();
      for($i = 2; $i <= (count($data_for_comparing_remove_arrays)); $i+=2){
        $pieces1 = explode('-', $data_for_comparing_remove_arrays[$i-2]);
        $pieces2 = explode('-',  $data_for_comparing_remove_arrays[$i-1]);

        $start_time = new DateTime($pieces1[0].'-'.$pieces1[1].'-'.$pieces1[2].' '.$pieces1[3]);
        if($data_for_comparing_remove_arrays[$i-2] != 0 AND $data_for_comparing_remove_arrays[$i-2] != $date.' 09:00'){
          $start_time->modify('+'.$minimum_time_beetwen_bookings_hours.' hours');
          $start_time->modify('+'.$minimum_time_beetwen_bookings_minutes.' minutes');

        }

        $end_time = new DateTime($pieces2[0].'-'.$pieces2[1].'-'.$pieces2[2].' '.$pieces2[3] );
        if( $data_for_comparing_remove_arrays[$i-1] != count($data_for_comparing_remove_arrays) AND $data_for_comparing_remove_arrays[$i-1] != $next_date.' 09:00'){
          $end_time->modify('-'.$minimum_time_beetwen_bookings_hours.' hours');
          $end_time->modify('-'.$minimum_time_beetwen_bookings_minutes.' hours');
        }

        $data_for_comparing_key_values[$start_time->format('Y-m-d H:i')] = $end_time->format('Y-m-d H:i');
      }
      // var_dump($data_for_comparing_key_values);

      // Check if customer time range is in sitter time range
      $selected_time_is_in_range = false;

      // var_dump($time_range_start->format('Y-m-d H:i'), $time_range_ends->format('Y-m-d H:i'));

      foreach ($data_for_comparing_key_values as $key => $value) {
        $key_time = new DateTime($key);
        $value_time = new DateTime($value);
        if($time_range_start->getTimestamp() >= $key_time->getTimestamp() AND $time_range_ends->getTimestamp() <= $value_time->getTimestamp() ){
          $selected_time_is_in_range = true;
        }
      }
      // var_dump($selected_time_is_in_range, $max_daily_sitting_time);


      // If customer time range is in sitter time range and customer duration is less or equal of sitter free hours and time is not in past
      // Show sitter
      if( $selected_time_is_in_range AND $duration <= $max_daily_sitting_time ){

        $children_url_data = '';
        foreach($children_age as $key => $child_age){
          $children_url_data.= "&num_of_child=".count($children_age)."&child_".($key+1)."_lang=".$children_language[$key]."&child_".($key+1)."_age=".$child_age;
        }

        // Booking url
        $url = "booking/?location=".$city.
        "&duration=".$duration.
        "&time=".$timefrom.
        "&date=".$date.
        "&sitter_id=".get_the_ID().
        $children_url_data;

        // Search results
        echo '<li>' . get_the_title() ."$" . $total_price . "<a href='$url'>BOOK</a></li>";
      } // End if show sitter

    } // End while loop

    echo '</ul></div>';

    wp_reset_postdata();
  } else {
    // no posts found
  }
  exit;
}

