(function($){
$(document).ready(function(){

  var search_obj = {

    // Variables
    system_search_form: $('.system_search_form'),
    on_change_fields: '.system_search_city_field2, .system_search_duration_field, .system_search_child_language_field, .system_search_child_range_field, .system_search_city_timefrom, .system_search_datepicker_field',
    datepicker_element: $('.system_search_datepicker_field'),
    add_child_element: $('.system_search_add_chield'),
    city_field: $('.system_search_city_field'),


    ///
    // Init method
    ///
    init: function(){
      this.get_languages_for_current_city();
      this.set_GET_params_in_url();
      this.add_child();
      this.remove_child();
      this.on_submit();
      this.on_change();
      this.system_search_form.trigger('submit');
    },


    ///
    // Handle form submit
    ///
    on_submit: function(){
      var $this = this;
      this.system_search_form.on('submit', function(){

        // Collect form data
        var city = $('.system_search_city_field').val();
        var duration = $('.system_search_duration_field').val();
        var date = $('.system_search_datepicker_field').val();
        var timefrom = $('.system_search_city_timefrom').val();
        var children_language = $('.system_search_child_language_field');
        var children_age = $('.system_search_child_range_field');

        // Collect children language and age
        var childrens = [];
        if(children_language.length == children_age.length){
          for(var i=0; i < children_language.length ; i++){
            childrens.push( [children_language.eq(i).val(), children_age.eq(i).val()] );
          }
        }

        // Collect data for ajax field
        var data = {
          'action': 'system_search_action',
          'search_nonce':ajax_object.search_nonce,
          'date': date,
          'timefrom': timefrom,
          'city': city,
          'duration': duration,
          'childrens': childrens
        };

        // Call ajax method
        $.post(ajax_object.ajax_url, data, function(response) {

          // Clean previous and append new search results
          $('.search-results').empty().append(response);
          console.log(response);

        })
        .fail(function() {
          alert( "Error, try again" );
        });

        return false;
      });
    },


    ///
    // When form fields change
    ///
    on_change: function(){
      var $this = this;

      // Trigger search on field change
      $('body').on('change', this.on_change_fields, function(){

        // Update get params
        $this.set_GET_params_in_url();

        // Triger submit when fields change
        $this.system_search_form.trigger('submit');
      });
    },


    ///
    // Add child function
    ///
    add_child: function(){
      var $this = this;
      $('body').on('click', '.system_search_add_child', function(e){
        e.preventDefault();

        // Get child_language_containers
        var child_language_container = $('.child_language_container'),
            child_language_container_length = child_language_container.length;

        // Clone last child_language_container and add it to DOM after it.
        if( child_language_container_length > 0 && child_language_container_length < 3 ){
          var current_child_container = child_language_container.eq(child_language_container_length - 1);

          // Remove child handler
          var remove_child = function(e){
            e.preventDefault();

            // If previous element is not first of all children
            if($(this).closest('.child_language_container').prev().index() != 0){
              $(this).closest('.child_language_container').prev().append($('<a href="#" class="remove_child">REMOVE</a>').on('click', remove_child));
            }
            $(this).closest('.child_language_container').remove();

            // Update GET params
            $this.set_GET_params_in_url();

            // trigger submit when children removed
            $this.system_search_form.trigger('submit');
          }

          // Add new children container
          current_child_container
          .find('.remove_child').remove().end()
          .clone()
          .append($('<a href="#" class="remove_child">REMOVE</a>').on('click',remove_child))
          .find('.system_search_child_language_field')
          .attr('name','system_search_child_language_field_' + (child_language_container_length+1))
          .end()
          .find('.system_search_child_range_field')
          .attr('name','system_search_child_range_field_' + (child_language_container_length+1))
          .end()
          .insertAfter( current_child_container );

        }

        // Update GET params
        $this.set_GET_params_in_url();

        // Trigger submit when children added
        $this.system_search_form.trigger('submit');
        return false;
      });
    },


    ///
    // Remove initial_child function, this function is called only one time when page loads.
    ///
    remove_child: function(){
      var $this = this;

      // Remove child handler
      var remove_child = function(e){
        e.preventDefault();

        // If previous element is not first of all children
        if($(this).closest('.child_language_container').prev().index() != 0){
          $(this).closest('.child_language_container').prev().append($('<a href="#" class="remove_child">REMOVE</a>').on('click', remove_child));
        }
        $(this).closest('.child_language_container').remove();

        // Update GET params
        $this.set_GET_params_in_url();

        // Trigger submit when children removed
        $this.system_search_form.trigger('submit');
      }

      $('.remove_child').on('click', remove_child);

    },


    ///
    // Set GET params handler
    ///
    set_GET_params_in_url: function(){

      // Get form fields values
      var city = $('.system_search_city_field').val(),
      duration = $('.system_search_duration_field').val(),
      date = $('.system_search_datepicker_field').val(),
      timefrom = $('.system_search_city_timefrom').val(),

      // Collect children information
      children_language = $('.system_search_child_language_field'),
      children_age = $('.system_search_child_range_field'),
      num_of_child = children_language.length;

      // Generate children url
      children_url_data = "&num_of_child=" + num_of_child;
      for (var i = 1; i <= num_of_child; i++) {
          children_url_data+= "&child_" + i + "_lang=" + children_language[i-1].value +
          "&child_" + i + "_age=" + children_age[i-1].value;
      }

      // Update get params with new value
      history.pushState(null, null, '?location=' + city + '&duration=' + duration  + '&time=' + timefrom
       + '&date=' + date +  children_url_data );

    },


    ///
    // Get languages for current city
    ///
    get_languages_for_current_city: function(){
      var $this = this;
      this.city_field.on('change', function(){

        // Collect form data
        var city = $('.system_search_city_field').val();
        var duration = $('.system_search_duration_field').val();
        var date = $('.system_search_datepicker_field').val();
        var timefrom = $('.system_search_city_timefrom').val();
        var children_language = $('.system_search_child_language_field');
        var children_age = $('.system_search_child_range_field');

        // Collect children language and age
        var childrens = [];
        if(children_language.length == children_age.length){
          for(var i=0; i < children_language.length ; i++){
            childrens.push( [children_language.eq(i).val(), children_age.eq(i).val()] );
          }
        }

        // Collect data for ajax field
        var data = {
          'action': 'system_get_languages_by_city_action',
          'search_nonce':ajax_object.search_nonce,
          'date': date,
          'timefrom': timefrom,
          'city': city,
          'duration': duration,
          'childrens': childrens
        };

        // Call ajax method
        $.post(ajax_object.ajax_url, data, function(response) {
          var language_field = $('.system_search_child_language_field');

          language_field.each(function(){
            // Change selected language with response from backend
            $(this).find('option').each(function(){
              if( $(this).text() == response ){
                $(this).siblings().removeAttr('selected');
                $(this).attr('selected','selected');
              }
            });
          })

          // Trigger change
          language_field.eq(0).change();

        })
        .fail(function() {
          alert( "Error, try again" );
        });

        return false;

      });
    }

  }


  ///
  // Initiate search object
  ///
  search_obj.init();



});
}(jQuery));
